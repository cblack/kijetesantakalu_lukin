package main

import (
	"strconv"
	"strings"
	"sync"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

type Operation struct {
	Row  int
	Col  int
	Text string
}

type Row struct {
	Row     int
	channel chan Operation
}

var operations = make(chan Operation, 32)
var textChan = make(chan string, 32)
var rows = 1
var rowMutex sync.Mutex

func AllocateRow() (ret Row) {
	rowMutex.Lock()
	defer rowMutex.Unlock()
	ret = Row{rows, operations}
	operations <- Operation{rows, 0, "..."}
	operations <- Operation{rows, 1, "..."}
	operations <- Operation{rows, 2, "..."}
	rows++
	return
}

func (r Row) SetGroup(text string) {
	r.channel <- Operation{
		r.Row,
		0,
		text,
	}
}

func (r Row) SetRepoCount(count int64) {
	r.channel <- Operation{
		r.Row,
		1,
		strconv.FormatInt(count, 10),
	}
}

func (r Row) SetCurrentRepo(count int64) {
	r.channel <- Operation{
		r.Row,
		2,
		strconv.FormatInt(count, 10),
	}
}

func (r Row) Error() {
	r.channel <- Operation{
		r.Row,
		1,
		"[red]ERROR",
	}
	r.channel <- Operation{
		r.Row,
		2,
		"[red]ERROR",
	}
}

func Output(text string) {
	textChan <- text
}

func Dashboard() {
	app := tview.NewApplication()

	repoTable := tview.NewTable()
	repoTable.SetCellSimple(0, 0, "[yellow]Group ")
	repoTable.SetCellSimple(0, 1, "[yellow]# of Repos ")
	repoTable.SetCellSimple(0, 2, "[yellow]Repo # ")
	repoTable.SetTitle("kijetesantakalu repo statistics")
	repoTable.SetBorder(true)
	repoTable.SetSeparator(tview.Borders.Vertical)

	go func() {
		for {
			select {
			case op := <-operations:
				repoTable.SetCell(op.Row, op.Col, tview.NewTableCell(op.Text).SetExpansion(1))
				app.Draw()
			}
		}
	}()

	textView := tview.NewTextView()
	textView.SetTitle("output")
	textView.SetBorder(true)
	textView.SetScrollable(true)

	go func() {
		for {
			select {
			case text := <-textChan:
				if textView.GetText(true) == "" {
					textView.SetText(text)
				} else {
					textView.SetText(strings.Join([]string{textView.GetText(false), text}, "\n"))
				}
				app.Draw()
			}
		}
	}()

	pages := tview.NewPages()
	pages.AddPage("Repos", repoTable, true, true)
	pages.AddPage("Output", textView, true, true)

	var page = "Repos"
	pages.SwitchToPage(page)

	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyTab {
			if page == "Repos" {
				page = "Output"
			} else {
				page = "Repos"
			}
			pages.SwitchToPage(page)
		}
		return event
	})

	app.SetRoot(pages, true).Run()
}
