package main

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func init() {
	RegisterHandler(StartFancyConfig)
	RegisterQueryHandler(DoneHandler)
	RegisterQueryHandler(ViewHandler)
	RegisterQueryHandler(BackHandler)
	RegisterQueryHandler(ConfigHandler)
	RegisterQueryHandler(TypesHandler)
	RegisterQueryHandler(GroupsHandler)
	RegisterQueryHandler(ProjectsHandler)
}

func SimpleReply(m *tgbotapi.Message, text string, mkup tgbotapi.InlineKeyboardMarkup) {
	msg := tgbotapi.NewMessage(m.Chat.ID, text)
	msg.ReplyMarkup = mkup
	telegramBot.queue <- msg
}

var backKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"« Back",
			"back",
		),
	),
)

var configKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"Set subscribed groups",
			"config>groups",
		),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"Set subscribed projects",
			"config>projects",
		),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"Manage event types",
			"config>types",
		),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"« Back",
			"back",
		),
	),
)

func ConfigHandler(q *tgbotapi.CallbackQuery, m *tgbotapi.Message) {
	if q.Data == "config" {
		msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Configuration options:")
		msg.ReplyMarkup = &configKeyboard

		telegramBot.Send(msg)
	}
}

func cleanInput(in string) (jsonOut string) {
	fields := strings.Split(in, ",")
	var cleaned []string
	for _, field := range fields {
		cleaned = append(cleaned, strings.TrimSpace(field))
	}
	data, _ := json.Marshal(fields)
	jsonOut = string(data)
	return
}

var configBackKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"« Back to Config",
			"config",
		),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"« Back to Home",
			"back",
		),
	),
)

func GroupsHandler(q *tgbotapi.CallbackQuery, m *tgbotapi.Message) {
	if q.Data == "config>groups" {
		msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Please send me a comma-separated list of groups you want to subscribe to within 1 minute, or send cancel to cancel")
		telegramBot.Send(msg)
		resp, ok := telegramBot.AwaitResponse(m, q.From, time.Minute)
		if resp == "cancel" {
			msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Cancelled.")
			msg.ReplyMarkup = &configBackKeyboard
			telegramBot.Send(msg)
			return
		}
		if !ok {
			msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Timed out.")
			msg.ReplyMarkup = &configBackKeyboard
			telegramBot.Send(msg)
		} else {
			chat := GetChat(m.Chat.ID)
			chat.UpdateTypeData(cleanInput(resp))
			msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Updated groups!")
			msg.ReplyMarkup = &configBackKeyboard
			telegramBot.Send(msg)
		}
	}
}

func ProjectsHandler(q *tgbotapi.CallbackQuery, m *tgbotapi.Message) {
	if q.Data == "config>projects" {
		msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Please send me a comma-separated list of projects you want to subscribe to within 1 minute, or send cancel to cancel")
		telegramBot.Send(msg)
		resp, ok := telegramBot.AwaitResponse(m, q.From, time.Minute)
		if resp == "cancel" {
			msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Cancelled.")
			msg.ReplyMarkup = &configBackKeyboard
			telegramBot.Send(msg)
			return
		}
		if !ok {
			msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Timed out.")
			msg.ReplyMarkup = &configBackKeyboard
			telegramBot.Send(msg)
		} else {
			chat := GetChat(m.Chat.ID)
			chat.UpdateProjectData(cleanInput(resp))
			msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Updated projects!")
			msg.ReplyMarkup = &configBackKeyboard
			telegramBot.Send(msg)
		}
	}
}

func TypesHandler(q *tgbotapi.CallbackQuery, m *tgbotapi.Message) {
	positiveButton := func(text, typ string) tgbotapi.InlineKeyboardButton {
		return tgbotapi.NewInlineKeyboardButtonData(
			fmt.Sprintf("✅ %s", text),
			"config>types:disable,"+typ,
		)
	}
	negativeButton := func(text, typ string) tgbotapi.InlineKeyboardButton {
		return tgbotapi.NewInlineKeyboardButtonData(
			fmt.Sprintf("❌ %s", text),
			"config>types:enable,"+typ,
		)
	}
	decideButton := func(text, typ string, ok bool) tgbotapi.InlineKeyboardButton {
		if ok {
			return positiveButton(text, typ)
		} else {
			return negativeButton(text, typ)
		}
	}
	if strings.HasPrefix(q.Data, "config>types") {
		var types []string
		json.Unmarshal([]byte(GetChat(m.Chat.ID).TypeData), &types)
		if strings.Contains(q.Data, ":") {
			msg := strings.TrimPrefix(q.Data, "config>types:")
			switch {
			case strings.HasPrefix(msg, "disable"):
				typ := strings.TrimPrefix(msg, "disable,")
				var trueTypes []string
				for _, eType := range types {
					if eType != typ {
						trueTypes = append(trueTypes, eType)
					}
				}
				chat := GetChat(m.Chat.ID)
				data, _ := json.Marshal(&trueTypes)
				chat.UpdateTypeData(string(data))
				types = trueTypes
			case strings.HasPrefix(msg, "enable"):
				types = append(types, strings.TrimPrefix(msg, "enable,"))
				chat := GetChat(m.Chat.ID)
				data, _ := json.Marshal(&types)
				chat.UpdateTypeData(string(data))
			}
		}
		chatKB := tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				decideButton("Issue/MR Created", "created", Contains(types, "created")),
				decideButton("Merge Request Merged", "merged", Contains(types, "merged")),
			),
			tgbotapi.NewInlineKeyboardRow(
				decideButton("Issue/MR Closed", "closed", Contains(types, "closed")),
				decideButton("Issue/MR Updated", "updated", Contains(types, "updated")),
			),
			tgbotapi.NewInlineKeyboardRow(
				decideButton("New Commits", "pushed", Contains(types, "pushed")),
				decideButton("Work Branch Commits", "work", Contains(types, "work")),
			),
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData(
					"« Back",
					"config",
				),
			),
		)

		msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Here are your type settings:")
		msg.ParseMode = "HTML"
		msg.ReplyMarkup = &chatKB

		telegramBot.Send(msg)
	}
}

func ViewHandler(q *tgbotapi.CallbackQuery, m *tgbotapi.Message) {
	if q.Data == "view" {
		chat := GetChat(m.Chat.ID)
		var groups, projects, types []string
		json.Unmarshal([]byte(chat.GroupData), &groups)
		json.Unmarshal([]byte(chat.ProjectData), &projects)
		json.Unmarshal([]byte(chat.TypeData), &types)

		var sb strings.Builder

		if len(groups) > 0 {
			sb.WriteString("You're currently subscribed to the following groups...\n")
			for _, group := range groups {
				sb.WriteString(fmt.Sprintf("- %s\n", group))
			}
		} else {
			sb.WriteString("You aren't subscribed to any groups...\n")
		}
		if len(projects) > 0 {
			sb.WriteString("...and you're subscribed to the following repositories...\n")
			for _, project := range projects {
				sb.WriteString(fmt.Sprintf("- %s\n", project))
			}
		} else {
			sb.WriteString("...and you aren't subscribed to any projects...")
		}
		if len(types) > 0 {
			sb.WriteString("...and you're subscribed to the following event types:\n")
			for _, typ := range types {
				sb.WriteString(fmt.Sprintf("- %s\n", typ))
			}
		} else {
			sb.WriteString("... and you aren't subscribed to any event types.")
		}

		msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, sb.String())
		msg.ParseMode = "HTML"
		msg.ReplyMarkup = &backKeyboard

		telegramBot.Send(msg)
	}
}

func DoneHandler(q *tgbotapi.CallbackQuery, m *tgbotapi.Message) {
	if q.Data == "done" {
		msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Ok then, all done!")
		telegramBot.Send(msg)
	}
}

var mainKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"Manage subscriptions",
			"config",
		),
		tgbotapi.NewInlineKeyboardButtonData(
			"View subscriptions",
			"view",
		),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(
			"Nothing",
			"done",
		),
	),
)

func BackHandler(q *tgbotapi.CallbackQuery, m *tgbotapi.Message) {
	if q.Data == "back" {
		msg := tgbotapi.NewEditMessageText(m.Chat.ID, m.MessageID, "Hi! I'm the kijetesantakalu lukin (observant raccoon). How can I help you today?")
		msg.ReplyMarkup = &mainKeyboard
		telegramBot.Send(msg)
	}
}

func StartFancyConfig(m *tgbotapi.Message) {
	switch m.Command() {
	case "start", "config":
		SimpleReply(m, "Hi! I'm the kijetesantakalu lukin (observant raccoon). How can I help you today?", mainKeyboard)
	}
}
