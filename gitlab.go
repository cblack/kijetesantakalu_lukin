package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	"golang.org/x/time/rate"
)

var client *gitlab.Client

var sort = "asc"

type Event struct {
	Type    string
	Group   string
	Project *gitlab.Project
	*gitlab.ContributionEvent
}

var pushEventsChan = make(chan Event, 100)
var mergeEventsChan = make(chan Event, 100)
var mergeRequestEventsChan = make(chan Event, 100)
var limiter = rate.NewLimiter(1, 60)
var ctx = context.Background()

func readIOCloser(i io.ReadCloser) string {
	buf := bytes.Buffer{}
	buf.ReadFrom(i)
	return buf.String()
}

func outputErrorBody(err error, resp *gitlab.Response) {
	bod := readIOCloser(resp.Body)
	Output(fmt.Sprintf("[red]error[white] ==>%+v\n[cyan]data[white]  ==>%+v\nbody:\n%s", err, resp, bod))
}

var projectCache = map[int]*gitlab.Project{}

func getProject(ID int) *gitlab.Project {
	if val, ok := projectCache[ID]; ok {
		return val
	}
	limiter.Wait(ctx)
	proj, resp, err := client.Projects.GetProject(ID, nil)
	if err != nil {
		outputErrorBody(err, resp)
		return nil
	}
	projectCache[ID] = proj
	return proj
}

func ChanEvents(evType string, evChan chan Event, evs []*gitlab.ContributionEvent) {
	for _, ev := range evs {
		if proj := getProject(ev.ProjectID); proj != nil {
			if proj.Namespace != nil {
				evChan <- Event{evType, proj.Namespace.Path, proj, ev}
			}
		}
	}
}

func EventID(ev Event) string {
	switch ev.TargetType {
	case "MergeRequest":
		return fmt.Sprintf("!%d", ev.TargetIID)
	case "Issue":
		return fmt.Sprintf("!%d", ev.TargetIID)
	}
	return ""
}

func EventURL(ev Event) string {
	switch ev.TargetType {
	case "MergeRequest":
		return fmt.Sprintf(
			"%s/-/merge_requests/%d",
			ev.Project.WebURL,
			ev.TargetIID,
		)
	case "Issue":
		return fmt.Sprintf(
			"%s/-/issues/%d",
			ev.Project.WebURL,
			ev.TargetIID,
		)
	}
	return ""
}

func GitlabReader() {
	sort := "desc"
	scope := "all"
	types := []gitlab.EventTypeValue{"created", "merged", "closed", "updated", "pushed"}
	chanForType := func(typ string) chan Event {
		switch typ {
		case "created", "updated", "closed":
			return mergeRequestEventsChan
		case "merged":
			return mergeEventsChan
		case "pushed":
			return pushEventsChan
		}
		return nil
	}
	for {
		time.Sleep(3 * time.Second)
		for _, evType := range types {
			limiter.Wait(ctx)
			evs, resp, err := client.Events.ListCurrentUserContributionEvents(&gitlab.ListContributionEventsOptions{Action: &evType, Sort: &sort, Scope: &scope})
			if err != nil {
				outputErrorBody(err, resp)
				continue
			}
			ChanEvents(string(evType), chanForType(string(evType)), evs)
		}
	}
}

func GitLab() {
	var err error
	client, err = gitlab.NewClient(config.GitLab.Token, gitlab.WithBaseURL(fmt.Sprintf("https://%s/api/v4", config.GitLab.URL)))
	if err != nil {
		panic(err)
	}
	Output("GitLab handler is now running...")
	go GitlabReader()
	broadcast := func(ev Event, em Embed, kb Keyboard) {
		var loadedConfigs []ChannelConfig
		for _, chat := range GetChats() {
			var conf ChannelConfig
			json.Unmarshal([]byte(chat.TypeData), &conf.EventTypes)
			json.Unmarshal([]byte(chat.GroupData), &conf.Groups)
			json.Unmarshal([]byte(chat.ProjectData), &conf.Projects)
			conf.ID = chat.ID
			loadedConfigs = append(loadedConfigs, conf)
		}
		for _, channel := range append(config.Telegram.Channels, loadedConfigs...) {
			if Contains(channel.Groups, ev.Group) || Contains(channel.Projects, ev.Project.PathWithNamespace) {
				if Contains(channel.EventTypes, ev.Type) {
					if strings.HasPrefix(ev.PushData.Ref, "work/") {
						if Contains(channel.EventTypes, "work") {
							telegramBot.SendMessage(channel.ID, em, kb)
						}
					} else {
						telegramBot.SendMessage(channel.ID, em, kb)
					}
				}
			}
		}
	}
	for {
		select {
		case event := <-mergeRequestEventsChan:
			if !TrackEvent(event.ContributionEvent) {
				continue
			}
			data, keyboard := Embed{
				Title: EmbedHeader{
					Text: fmt.Sprintf("%s %s #%d at %s", event.Author.Name, event.ActionName, event.TargetIID, event.Project.NameWithNamespace),
				},
				Fields: []EmbedField{
					{
						Title: "Title",
						Body:  event.TargetTitle,
					},
				},
			}, Keyboard{
				Title: "View on " + config.GitLab.URL,
				URL:   EventURL(event),
			}
			broadcast(event, data, keyboard)
		case event := <-mergeEventsChan:
			if !TrackEvent(event.ContributionEvent) {
				continue
			}
			data, keyboard := Embed{
				Title: EmbedHeader{
					Text: fmt.Sprintf("%s %s #%d at %s", event.Author.Name, event.ActionName, event.TargetIID, event.Project.NameWithNamespace),
				},
				Fields: []EmbedField{
					{
						Title: "Title",
						Body:  event.TargetTitle,
					},
				},
			}, Keyboard{
				Title: "View on " + config.GitLab.URL,
				URL:   EventURL(event),
			}
			broadcast(event, data, keyboard)
		case event := <-pushEventsChan:
			if strings.HasPrefix(event.PushData.CommitTitle, "GIT_SILENT") ||
				strings.HasPrefix(event.PushData.CommitTitle, "SVN_SILENT") {
				continue
			}
			if event.PushData.CommitTitle == "" || event.PushData.CommitTo == "" {
				continue
			}
			if !TrackCommit(event.PushData.CommitTo, event.Project.NameWithNamespace) {
				continue
			}
			data, keyboard := Embed{
				Title: EmbedHeader{
					Text: fmt.Sprintf("New commit from %s in %s", event.Author.Name, event.Project.NameWithNamespace),
				},
				Fields: []EmbedField{
					{
						Title: "Title",
						Body:  event.PushData.CommitTitle,
					},
					{
						Title: "Branch",
						Body:  event.PushData.Ref,
					},
				},
			}, Keyboard{
				Title: "View on " + config.GitLab.URL,
				URL: fmt.Sprintf(
					"%s/-/commit/%s",
					event.Project.WebURL,
					event.PushData.CommitTo,
				),
			}
			broadcast(event, data, keyboard)
		}
	}
}
