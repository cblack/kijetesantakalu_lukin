package main

import (
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var telegramBot Bot

type Handler func(m *tgbotapi.Message)
type QueryHandler func(q *tgbotapi.CallbackQuery, m *tgbotapi.Message)

var handlers []Handler
var queryHandlers []QueryHandler

func RegisterHandler(f Handler) {
	handlers = append(handlers, f)
}

func RegisterQueryHandler(q QueryHandler) {
	queryHandlers = append(queryHandlers, q)
}

func main() {
	go Dashboard()
	Output("Initializing kijetesantakalu lukin...")

	Output("Logging in...")
	bot, err := tgbotapi.NewBotAPI(config.Telegram.Token)
	telegramBot = NewBot(bot)
	if err != nil {
		Output("Error creating Telegram session: " + err.Error())
		return
	}
	Output("Logged in!")

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, _ := telegramBot.GetUpdatesChan(u)

	Output("kijetesantakalu lukin is now running.")
	go telegramBot.QueueLoop()
	defer db.Close()
	go GitLab()
	for update := range updates {
		if update.CallbackQuery != nil && update.CallbackQuery.Message != nil {
			for _, handler := range queryHandlers {
				go handler(update.CallbackQuery, update.CallbackQuery.Message)
			}
			TelegramPaginatorHandler(update.CallbackQuery.Message.MessageID, update.CallbackQuery.Data)
		}
		if update.Message != nil {
			for _, handler := range onetimeMessageHandlers {
				handler.handler(update.Message)
				removeTelegramHandler(handler)
			}
			for _, handler := range handlers {
				go handler(update.Message)
			}
			if update.Message.ForwardFromChat != nil {
				Output(strconv.FormatInt(update.Message.ForwardFromChat.ID, 10))
			}
		}
	}
}
