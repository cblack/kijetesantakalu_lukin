package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/xanzy/go-gitlab"
)

type Chat struct {
	ID          int64 `gorm:"primary_key,auto_increment:false"`
	ProjectData string
	GroupData   string
	TypeData    string
}

type PostedCommit struct {
	CommitHash           string `gorm:"primary_key"`
	ProjectWithNamespace string `gorm:"primary_key"`
}

type PostedEvent struct {
	AuthorID  int64
	CreatedAt string
}

var db *gorm.DB

func init() {
	var err error
	db, err = gorm.Open("sqlite3", "settings.db")
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(&Chat{})
	db.AutoMigrate(&PostedCommit{})
	db.AutoMigrate(&PostedEvent{})
}

func TrackEvent(ev *gitlab.ContributionEvent) bool {
	if ev == nil || ev.CreatedAt == nil {
		return false
	}
	var event PostedEvent
	db.First(
		&event,
		"author_id = ?", ev.AuthorID,
		"created_at = ?", ev.ActionName,
	)
	if (event == PostedEvent{}) {
		db.Create(PostedEvent{int64(ev.AuthorID), ev.CreatedAt.String()})
		return true
	}
	return false
}

func TrackCommit(hash, project string) bool {
	var commit PostedCommit
	db.First(&commit, "commit_hash = ?", hash, "project_with_namespace = ?", project)
	if (commit == PostedCommit{}) {
		db.Create(PostedCommit{hash, project})
		return true
	}
	return false
}

func GetChats() (ret []Chat) {
	db.Find(&ret)
	return
}

func GetChat(ID int64) (ret Chat) {
	db.First(&ret, "id = ?", ID)
	if (ret == Chat{}) {
		db.Create(Chat{ID, "", "", ""})
		ret = Chat{ID, "", "", ""}
	}
	return
}

func (c *Chat) UpdateProjectData(data string) {
	db.Model(*c).Update("project_data", data)
	c.ProjectData = data
}

func (c *Chat) UpdateGroupData(data string) {
	db.Model(*c).Update("group_data", data)
	c.GroupData = data
}

func (c *Chat) UpdateTypeData(data string) {
	db.Model(*c).Update("type_data", data)
	c.TypeData = data
}
