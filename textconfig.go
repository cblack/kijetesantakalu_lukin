package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/alecthomas/repr"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func init() {
	RegisterHandler(UpdateSubscriptionHandler)
	RegisterHandler(UpdateTypeHandler)
	RegisterHandler(ShowConfigHandler)
}

func parse(m *tgbotapi.Message, prefix, suffix string) (bool, []string) {
	if !strings.HasPrefix(m.Text, prefix) || !strings.HasSuffix(m.Text, suffix) {
		return false, []string{}
	}
	split := strings.Split(strings.TrimPrefix(strings.TrimSuffix(strings.TrimSpace(m.Text), suffix), prefix), ",")
	var cleaned []string
	for _, str := range split {
		cleaned = append(cleaned, strings.TrimSuffix(strings.TrimPrefix(strings.TrimSpace(str), "\""), "\""))
	}
	return true, cleaned
}

func UpdateSubscriptionHandler(m *tgbotapi.Message) {
	if ok, data := parse(m, "chatData.subscriptions := []string{", "}"); ok {
		var groups []string
		var projects []string
		for _, cleanedStr := range data {
			if strings.Contains(cleanedStr, "/") {
				projects = append(projects, cleanedStr)
			} else {
				groups = append(groups, cleanedStr)
			}
		}
		gData, err := json.Marshal(groups)
		if err != nil {
			telegramBot.queue <- tgbotapi.NewMessage(m.Chat.ID, "Sorry, there was an error marshalling data: "+err.Error())
		}
		pData, err := json.Marshal(projects)
		if err != nil {
			telegramBot.queue <- tgbotapi.NewMessage(m.Chat.ID, "Sorry, there was an error marshalling data: "+err.Error())
		}
		chat := GetChat(m.Chat.ID)
		chat.UpdateGroupData(string(gData))
		chat.UpdateProjectData(string(pData))
		telegramBot.queue <- tgbotapi.NewMessage(m.Chat.ID, "Subscriptions updated!\n")
	}
}

func UpdateTypeHandler(m *tgbotapi.Message) {
	if ok, data := parse(m, "chatData.types := []string{", "}"); ok {
		json, err := json.Marshal(data)
		if err != nil {
			telegramBot.queue <- tgbotapi.NewMessage(m.Chat.ID, "Sorry, there was an error marshalling data: "+err.Error())
		}
		chat := GetChat(m.Chat.ID)
		chat.UpdateTypeData(string(json))
		telegramBot.queue <- tgbotapi.NewMessage(m.Chat.ID, "Subscription types updated!\n")
	}
}

func ShowConfigHandler(m *tgbotapi.Message) {
	if strings.HasPrefix(m.Text, "repr.Println(chatData)") {
		chat := GetChat(m.Chat.ID)
		msg := tgbotapi.NewMessage(m.Chat.ID, fmt.Sprintf("<pre><code>%s</code></pre>", repr.String(chat)))
		msg.ParseMode = "HTML"
		telegramBot.queue <- msg
	}
}
