package main

import (
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var onetimeMessageHandlers []*tgMessageHandlerInstance
var messageHandlerMutex = sync.Mutex{}

var onetimeQueryHandlers []*tgCallbackQueryHandlerInstance
var queryHandlerMutex = sync.Mutex{}

type tgMessageHandlerInstance struct {
	handler func(m *tgbotapi.Message)
}

type tgCallbackQueryHandlerInstance struct {
	handler func(q *tgbotapi.CallbackQuery, m *tgbotapi.Message)
}

func removeTelegramHandler(ehi *tgMessageHandlerInstance) {
	messageHandlerMutex.Lock()
	defer messageHandlerMutex.Unlock()
	for idx, handler := range onetimeMessageHandlers {
		if handler == ehi {
			onetimeMessageHandlers = append(onetimeMessageHandlers[:idx], onetimeMessageHandlers[idx+1:]...)
		}
	}
}

func addTelegramHandlerOnce(input func(m *tgbotapi.Message)) {
	messageHandlerMutex.Lock()
	defer messageHandlerMutex.Unlock()
	ehi := tgMessageHandlerInstance{input}
	onetimeMessageHandlers = append(onetimeMessageHandlers, &ehi)
}

func waitForTelegramMessage() chan *tgbotapi.Message {
	channel := make(chan *tgbotapi.Message)
	addTelegramHandlerOnce(func(m *tgbotapi.Message) {
		channel <- m
	})
	return channel
}

func handlerMessages(m *tgbotapi.Message) {
	messageHandlerMutex.Lock()
	defer messageHandlerMutex.Unlock()
	for _, handler := range onetimeMessageHandlers {
		handler.handler(m)
		removeTelegramHandler(handler)
	}
}

func (t *Bot) NextResponse(msg *tgbotapi.Message, from *tgbotapi.User) (out chan string) {
	out = make(chan string)
	go func() {
		for {
			select {
			case usermsg := <-waitForTelegramMessage():
				if usermsg.Chat.ID == msg.Chat.ID && usermsg.From.ID == from.ID {
					out <- usermsg.Text
					return
				}
			}
		}
	}()
	return out
}

func (t *Bot) AwaitResponse(msg *tgbotapi.Message, from *tgbotapi.User, tenpo time.Duration) (response string, ok bool) {
	timeoutChan := make(chan struct{})
	go func() {
		time.Sleep(tenpo)
		timeoutChan <- struct{}{}
	}()
	for {
		select {
		case msg := <-t.NextResponse(msg, from):
			return msg, true
		case <-timeoutChan:
			return "", false
		}
	}
}
